﻿<?xml version='1.0' encoding='UTF-8'?>
<LVClass LVVersion="17008000">
	<Property Name="" Type="Bool">true</Property>
	<Property Name="NI.Lib.ContainingLib" Type="Str">MemberVICreation.lvlib</Property>
	<Property Name="NI.Lib.ContainingLibPath" Type="Str">/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/MemberVICreation.lvlib</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!);!!!*Q(C=\&gt;4.&lt;=*!%-8RFSC(8/EAII68!L2!#^-#VRRJ96KA"6K9&amp;GC"&amp;JS`FUG#C%3E##EZ:*=V],Q@0[]N3[-]3U_;PCOLG_6?Z]],^7&amp;;4:^Z@Z]`Z[]_@"F`U7G[0D^`XC^HOD8_`/N[`:^@X\3[7@6@`MPPFN&gt;PXQ&amp;8Z@[%"]UP)FL2EB9U;VZK+P)C,`)C,`)C4`)E4`)E4`)E$`)A$`)A$`)A.\H*47ZSEZN]6(+2CVTEE)L&amp;CY7+39M*CMZ16"Q+4_%J0)7(PSI]B;@Q&amp;*\#1R=6HM*4?!J0Y7'9#E`B+4S&amp;J`!QV:$5K/2Y#A`4+`%EHM34?")03SLR*)"EM74C:")93EYG0R*0YEE]`&amp;4C34S**`%E(EYL]33?R*.Y%A^$RK\EU-S6(!`4+0!%HM!4?!)05SPQ"*\!%XA#$]MJ]!3?!"%M'%Q/1='AI%0Q*`!%(LY5?!*0Y!E]A9&gt;4YQ\&amp;W*F:-V&gt;S0-:D0-:D0-&lt;$&amp;$)?YT%?YT%?JJ8R')`R')`RM*3-RXC-RU$-ICQP-ZE:;$K:Q(DYD+@&amp;YS\FE(B5K2^?^5/J@ND5$Z([Y6$@&gt;08.6.]E^?;L.V7^7?J.5&amp;_='KX'K"&gt;2$ZY\[M4R3$P1^L1&gt;&lt;5P&lt;U.;U*7UR$\VTR^0JJ/0RK-0BI0V_L^VOJ_VWK]VGI`6[L?6SK=6C]@%;?+&amp;_P"$_QHPJ4RA?&gt;4EP?`1'4'SN=A!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">385908736</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.LVClass.ClassNameVisibleInProbe" Type="Bool">true</Property>
	<Property Name="NI.LVClass.CoreWirePen" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!6$0%.M&gt;8.U:8)_$1I]4G&amp;N:4Z1:7Y],UZB&lt;75_$1I]4H6N27RU=TYY0#^/&gt;7V&amp;&lt;(2T0AU+0&amp;5T-DY.#DR/97VF0E:P=G6H=G^V&lt;G1A1W^M&lt;X)],UZB&lt;75_$1I]6G&amp;M0D%T-T9Z.41Y0#^797Q_$1I],V5T-DY.#DR6-T)_$1I]4G&amp;N:4Z#97.L:X*P&gt;7ZE)%.P&lt;'^S0#^/97VF0AU+0&amp;:B&lt;$YR.D=X.T)R.4QP6G&amp;M0AU+0#^6-T)_$1I]1WRV=X2F=DY.#DR/97VF0E:J&lt;'QA5'&amp;U&gt;'6S&lt;DQP4G&amp;N:4Y.#DR/&gt;7V&amp;&lt;(2T0DA],UZV&lt;56M&gt;(-_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A-$QP4G&amp;N:4Y.#DR797Q_-45],V:B&lt;$Y.#DQP64A_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A-4QP4G&amp;N:4Y.#DR797Q_-45],V:B&lt;$Y.#DQP64A_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A-DQP4G&amp;N:4Y.#DR797Q_-45],V:B&lt;$Y.#DQP64A_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A-TQP4G&amp;N:4Y.#DR797Q_-45],V:B&lt;$Y.#DQP64A_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A.$QP4G&amp;N:4Y.#DR797Q_-45],V:B&lt;$Y.#DQP64A_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A.4QP4G&amp;N:4Y.#DR797Q_-45],V:B&lt;$Y.#DQP64A_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A.DQP4G&amp;N:4Y.#DR797Q_-45],V:B&lt;$Y.#DQP64A_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A.TQP4G&amp;N:4Y.#DR797Q_-45],V:B&lt;$Y.#DQP64A_$1I],U.M&gt;8.U:8)_$1I]34%W0AU+0%ZB&lt;75_6WFE&gt;'A],UZB&lt;75_$1I]6G&amp;M0D%],V:B&lt;$Y.#DQP34%W0AU+0%680AU+0%ZB&lt;75_47^E:4QP4G&amp;N:4Y.#DR$;'^J9W5_1W^Q?4QP1WBP;7.F0AU+0%.I&lt;WFD:4Z0=DQP1WBP;7.F0AU+0%.I&lt;WFD:4Z&amp;?'.M&gt;8.J&gt;G5A4X)],U.I&lt;WFD:4Y.#DR$;'^J9W5_1GFU)%.M:7&amp;S0#^$;'^J9W5_$1I]1WBP;7.F0EZP&gt;#"$&lt;X"Z0#^$;'^J9W5_$1I]1WBP;7.F0EZP&gt;#"0=DQP1WBP;7.F0AU+0%.I&lt;WFD:4Z/&lt;X)A28BD&lt;(6T;8:F)%^S0#^$;'^J9W5_$1I]1WBP;7.F0EZP&gt;#"#;81A1WRF98)],U.I&lt;WFD:4Y.#DR797Q_-$QP6G&amp;M0AU+0#^&amp;6TY.#DR&amp;4$Y.#DR/97VF0F.U?7RF0#^/97VF0AU+0%.I&lt;WFD:4Z4&lt;WRJ:$QP1WBP;7.F0AU+0%.I&lt;WFD:4Z%98.I0#^$;'^J9W5_$1I]1WBP;7.F0E2P&gt;$QP1WBP;7.F0AU+0%.I&lt;WFD:4Z%98.I)%2P&gt;$QP1WBP;7.F0AU+0%.I&lt;WFD:4Z%98.I)%2P&gt;#"%&lt;X1],U.I&lt;WFD:4Y.#DR797Q_-$QP6G&amp;M0AU+0#^&amp;4$Y.#DR&amp;4$Y.#DR/97VF0E:J&lt;'QA5H6M:4QP4G&amp;N:4Y.#DR$;'^J9W5_28:F&lt;C"0:'1],U.I&lt;WFD:4Y.#DR$;'^J9W5_6WFO:'FO:TQP1WBP;7.F0AU+0&amp;:B&lt;$YQ0#^797Q_$1I],U6-0AU+0%6-0AU+0%ZB&lt;75_27ZE)%.B=(-],UZB&lt;75_$1I]1WBP;7.F0E2F:G&amp;V&lt;(1],U.I&lt;WFD:4Y.#DR$;'^J9W5_2GRB&gt;$QP1WBP;7.F0AU+0&amp;:B&lt;$YQ0#^797Q_$1I],U6-0AU+0#^$&lt;(6T&gt;'6S0AU+!!!!!!</Property>
	<Property Name="NI.LVClass.EdgeWirePen" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!6$0%.M&gt;8.U:8)_$1I]4G&amp;N:4Z1:7Y],UZB&lt;75_$1I]4H6N27RU=TYY0#^/&gt;7V&amp;&lt;(2T0AU+0&amp;5T-DY.#DR/97VF0E:P=G6H=G^V&lt;G1A1W^M&lt;X)],UZB&lt;75_$1I]6G&amp;M0D%T-T9Z.41Y0#^797Q_$1I],V5T-DY.#DR6-T)_$1I]4G&amp;N:4Z#97.L:X*P&gt;7ZE)%.P&lt;'^S0#^/97VF0AU+0&amp;:B&lt;$YR.D=X.T)R.4QP6G&amp;M0AU+0#^6-T)_$1I]1WRV=X2F=DY.#DR/97VF0E:J&lt;'QA5'&amp;U&gt;'6S&lt;DQP4G&amp;N:4Y.#DR/&gt;7V&amp;&lt;(2T0DA],UZV&lt;56M&gt;(-_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A-$QP4G&amp;N:4Y.#DR797Q_.D!],V:B&lt;$Y.#DQP64A_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A-4QP4G&amp;N:4Y.#DR797Q_.D!],V:B&lt;$Y.#DQP64A_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A-DQP4G&amp;N:4Y.#DR797Q_.D!],V:B&lt;$Y.#DQP64A_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A-TQP4G&amp;N:4Y.#DR797Q_.D!],V:B&lt;$Y.#DQP64A_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A.$QP4G&amp;N:4Y.#DR797Q_.D!],V:B&lt;$Y.#DQP64A_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A.4QP4G&amp;N:4Y.#DR797Q_.D!],V:B&lt;$Y.#DQP64A_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A.DQP4G&amp;N:4Y.#DR797Q_.D!],V:B&lt;$Y.#DQP64A_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A.TQP4G&amp;N:4Y.#DR797Q_.D!],V:B&lt;$Y.#DQP64A_$1I],U.M&gt;8.U:8)_$1I]34%W0AU+0%ZB&lt;75_6WFE&gt;'A],UZB&lt;75_$1I]6G&amp;M0D-],V:B&lt;$Y.#DQP34%W0AU+0%680AU+0%ZB&lt;75_47^E:4QP4G&amp;N:4Y.#DR$;'^J9W5_1W^Q?4QP1WBP;7.F0AU+0%.I&lt;WFD:4Z0=DQP1WBP;7.F0AU+0%.I&lt;WFD:4Z&amp;?'.M&gt;8.J&gt;G5A4X)],U.I&lt;WFD:4Y.#DR$;'^J9W5_1GFU)%.M:7&amp;S0#^$;'^J9W5_$1I]1WBP;7.F0EZP&gt;#"$&lt;X"Z0#^$;'^J9W5_$1I]1WBP;7.F0EZP&gt;#"0=DQP1WBP;7.F0AU+0%.I&lt;WFD:4Z/&lt;X)A28BD&lt;(6T;8:F)%^S0#^$;'^J9W5_$1I]1WBP;7.F0EZP&gt;#"#;81A1WRF98)],U.I&lt;WFD:4Y.#DR797Q_-$QP6G&amp;M0AU+0#^&amp;6TY.#DR&amp;4$Y.#DR/97VF0F.U?7RF0#^/97VF0AU+0%.I&lt;WFD:4Z4&lt;WRJ:$QP1WBP;7.F0AU+0%.I&lt;WFD:4Z%98.I0#^$;'^J9W5_$1I]1WBP;7.F0E2P&gt;$QP1WBP;7.F0AU+0%.I&lt;WFD:4Z%98.I)%2P&gt;$QP1WBP;7.F0AU+0%.I&lt;WFD:4Z%98.I)%2P&gt;#"%&lt;X1],U.I&lt;WFD:4Y.#DR797Q_-$QP6G&amp;M0AU+0#^&amp;4$Y.#DR&amp;4$Y.#DR/97VF0E:J&lt;'QA5H6M:4QP4G&amp;N:4Y.#DR$;'^J9W5_28:F&lt;C"0:'1],U.I&lt;WFD:4Y.#DR$;'^J9W5_6WFO:'FO:TQP1WBP;7.F0AU+0&amp;:B&lt;$YQ0#^797Q_$1I],U6-0AU+0%6-0AU+0%ZB&lt;75_27ZE)%.B=(-],UZB&lt;75_$1I]1WBP;7.F0E2F:G&amp;V&lt;(1],U.I&lt;WFD:4Y.#DR$;'^J9W5_2GRB&gt;$QP1WBP;7.F0AU+0&amp;:B&lt;$YQ0#^797Q_$1I],U6-0AU+0#^$&lt;(6T&gt;'6S0AU+!!!!!!</Property>
	<Property Name="NI.LVClass.FlattenedPrivateDataCTL" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!#1&gt;5F.31QU+!!.-6E.$4%*76Q!!([!!!!2^!!!!)!!!(Y!!!!!Y!!!!!B:.:7VC:8*735.S:7&amp;U;7^O,GRW&lt;'FC(%*B=W6"9W.F=X.P=F.D=GFQ&gt;'6S,GRW9WRB=X-!!!#A&amp;Q#!!!!Q!!!I!!1!!!!!"!!$!$Q!P!!@1)!#!!!!!!%!!1!'`````Q!!!!!!!!!!!!!!!-%'5)9HTH&gt;"P:"B9J:Y()Y!!!!-!!!!%!!!!!!W&gt;6P&amp;DF^&amp;1[3=$JH\PZR+V"W-W9]!MA4JA!G9\0B#@A!!%!!!!!!!!!!!!!!!!!!!!!!!!!!!!!%!!!$`````V"W-W9]!MA4JA!G9\0B#@A!!!"#D)+#4;5=R?B::*PD%FG&lt;+!!!!"!!!!!!!!!#I!!&amp;-6E.$!!!!!A!#6EF-1A!!!!"16%AQ!!!!"1!"!!%!!!!!!A!#6EF$1Q!!!!!"%5.-5V6*5&amp;^"9W.F=X-O9X2M5&amp;2)-!!!!&amp;%!!!!'#DRS:8.P&gt;8*D:4Y*2H*B&lt;76X&lt;X*L#6"S&lt;X:J:'6S=QZ-6E.M98.T4'FC=G&amp;S?1R/:8&gt;"9W.F=X.P=H-21UR465F18U&amp;D9W6T=SZD&gt;'Q!!!!$!!&amp;#!!!!!!!$!!!!!A!#!!!!!!!I!!!!)HC=9_"E9'ZAO-!!R)Q/4!V=1":4"A/9:PD!Q-!2Q!!!D&lt;M)6A!!!"1!!!!5?*RDY'$A9?"!A1Q!!X!!41!!!%M!!!%9?*RD9-!%`Y%!3$%S-$$^!.*M;/*A'M;G*M"F,C[\I/,-1-Q#R+QQ9;#\^Q"J*J!Y6)UY2)LJ!2#@1$?((UI`1")$!,LV+6]!!!!!$!!"6EF%5Q!!!!!!!Q!!!&lt;!!!!/=?*T&lt;Q-D!E'FM9=&lt;!R-$!$'3,-T1Q*/?HJ0)S!0E-%+!$9V!!!K$G;;'*'RYYH!9%?PTS,7"_]RO?&lt;B=6A?9;&amp;1GG5J&amp;O(R724B]6FEY7F2&gt;``P``XXS%ZX#X2]ZR2RO1WGY/I0BR&amp;R5/%!&gt;)MY$I`Y%:)&amp;7IZMFU!G7"N!33"LC"+09(!&amp;6R.&amp;1I-Z3Q'"[)/HS]Q912YF#9%[+QO:&gt;Y]ZP@=!!^*8$Q)5NXIQ;1XTM22!+&amp;?$J$/#3/OX$IC!(ZD#&gt;!"H&lt;SQ(T.!@&gt;0'-C!%B7"4B/123S--)O[W9Y\;)$$Q5%%1G6!K!I)61#C&gt;I"&gt;=)1D\D!]`.?_PL?,&amp;5CT)=7*!R!XA"B-K&amp;C0A:'"%=RE:&amp;A,67M$:$."R7"R#W)L1)/.E=%?LO=W6&amp;Y$S2Q82JA?B,JK*(=QA=VA:0D$!$-0;"^54Q05X3!R8[$9!3A\"-C?!'6(!^E@I/QE)&amp;M!SMY%MAU9)?Q]+"NM'1.OWNH@R25JG-$Z!J9V6)%Y/&lt;@!Q%#POCAN1+=IT2_)Q]$933=VX-&lt;94C@;5#=YNB;I4A7ENC#ZD*"3!0U&lt;I*5!!!'Z!!!#Q(C==W"A9-AUND"D9'*A9':E9""H;'")TE^*:5!#.5Q-/%&amp;I=(B9]RO6\BI6C]Y;&amp;98O("7&lt;TBQ6G7\W&lt;F_'4B]6FEY7F2&gt;``P``X`S$E8`+!@\7"+!&gt;P?5MH35K#LWR,*UR1+8O,.W_,.D5GI$5VL'!T/X.:A%&lt;[]&lt;3\9V6,1=*ZNY"OLP8$S3FU"P-UBGC)N.1T=,!0`6!1RW)/NRRR_#)Q?%3&lt;PZN2ZI`-,\=!&amp;*?S!AWWIGRUU6&amp;*KX&lt;ET(.WANIWM&amp;3N6Y`*J"*T1?91&amp;,&gt;@ESN*UP%OCW\T9%;*)"9I0EYSWN*`GU(8W=&gt;&gt;V%2!!5;E*9!U9&lt;@$0^X-\1?+*(OFOBGNPT1_&lt;77$7C)2/@2ZM-MGM&gt;!GF],QR7`@=U/&amp;!;;ZNP[NEY.'-,=;*YL?1$VY&lt;9$T5&gt;5YO+D9Q_X@A0T4_Q!"A]$P_P"V]`1YG`N[XO\7)!U)Z+9!Q-I,4##R5"2LQ)6@]'!%!O!CCER3-$&amp;7K''P!3+-%(&amp;;K$K1/Q63/;$A,/`CSN[/A-:I1L%S&lt;E&amp;"A:[V56J!4J&amp;;@Z!(!&lt;'4DKJY4&lt;'&gt;DL2BDL"M&lt;6!&gt;1!5T&lt;VB!!!!!!!"C1!!!HRYH(.A9'$).,9Q?]$)Q-!-R/)-$1T*_3GJ$%D!BIE"*QA0;X[DU_WC9N(JIK,2\;.CU_GDIN,.U-E)&amp;.0I^7!"=E(#+LW",*UB1"E(FEZ(&amp;OL+&gt;4GQ&gt;!$FA)+^DIRA:\A!Z1)918*JX:[-;&lt;O9'.+;@T+H]4N&gt;4/M-A,K-#7:+]U%GM*M,G&amp;K`F1BWG`7[M1!.E?E7!_J8?-VVX%6&amp;!?22QUP&gt;D+U(/I_8M(2&lt;&gt;.N&lt;`CIR[A8+!&amp;7+!'E2)-U$.)5$K*K&amp;$;D[/)M+3./,0````W]^5*I%=F)X1_O"%N:OBO9$)!MEA(9L!,5)N*[M=Q3S:&gt;,YZ6U9',IZA8)MH4QK9+W&gt;RVK`F;K#:*M0ALQN!T2(!K9`2%8!#_B'A&gt;:,^3=.PX6_;$[C%R&gt;`'#V_VL[_NYM:3$-CC4E!=2:1"#1'CNIIK'10AQ&amp;9$)3FI')23/LE'2([1=$:X]560:W!F)#]HFS18+:88:17I&amp;/5ZA`%97$MJ*-;&lt;G.MJR.NK"-=7QM!EES;Y!!!!!!!!!Y8!9!#!!!'-4=O-#YR!!!!!!!!$"=!A!!!!!1R.SYQ!!!!!!Y8!9!#!!!'-4=O-#YR!!!!!!!!$"=!A!!!!!1R.SYQ!!!!!!Y8!9!#!!!'-4=O-#YR!!!!!!!!&amp;!%!!!$V6T7#?3;CD#ZT5EY'34G&gt;!!!!$1!!!!!!!!!!!!!!!!!!!!!!!!#!`````Y!!!!'!!!!"A!!!!9!!!!'!Y!!"A*!!!9$D:!'!F5I"A/0-!9!!!!'!!!!"A!!!!&lt;!!!!'Q!!!"S:EW\@ICJ+H*GWTJA!!!!9!!!!'!!!!"H!)!!;!!#!'9WMS:B2+J5&lt;D3R:'!!)!"A!!!!9!!!!'!!!!"A!!!!@````]!!!)!``````````````````````!!!!!!!!!!!!!!!!!!!!`Q!!!!!!!!!!!!!!!!!!!0]!!!!!!!!!!!!!!!!!!!$`!!!!!!!!!!!!!!!!!!!!`Q!!!!``!!!!!!!!!!!!!0]!!!!0!0!!!!!!!!!!!!$`!!!!$`]!$`$`!0!!!!!!`Q!!!!]!]0$Q]!]0!!!!!0]!!!!0`Q!0``!0]!!!!!$`!!!!!!!!!!!!!!!!!!!!`Q!!!!!!!!!!!!!!!!!!!0]!!!!!!!!!!!!!!!!!!!$`$`!!!!!!!!!!!!!!!!!!`Q`Q!!!!!!!!!!!!!!!!!0`Q$Q$`!0]!]!`Q`Q``$`$```]0!!]!$Q]0!0!0$Q]!``!0!0]!`Q`Q`Q`Q$`]0!0]!!!!!!!!!!!!!!!!!!!$`!!!!!!!!!!!!!!!!!!!!`Q!!!!!!!!!!!!!!!!!!!0]!``!!!!!0!!!!!!!!!!$`$Q!!!!!!!!!!$Q!!!!!!`Q$`!!`Q`Q]0]!`Q$Q$`!0]!!0$Q!0!0$Q]0!0$Q]!$`$`]!$`$Q$Q`Q!0$`!0!!`Q!!!!!!!!!0!!!!!!!!!0]!!!!!!!!!!!!!!!!!!!$`!!!!!!!!!!!!!!!!!!!!`Q!!!!!!!!!!!!!!!!!!!0]!!!!!!!!!!!!!!!!!!!$`````````````````````]!!!1!````````````````````````````````````````````!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!````!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!$`!!$`!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!0```Q!!!0``!0``!!$`!!!!!!!!!!!!``]!!!!!!!!!`Q!!`Q$`!0]!`Q!!`Q$`!!!!!!!!!!$``Q!!!!!!!!$```]!!!$`````!!$``Q!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$```]!!0]!!0``!!$``Q!!`Q!!``]!``]!````!0``!0```````Q$`!!!!`Q!!!0]!`Q$`!!$`!!$`!0]!`Q!!````!!$`!!$``Q!!``]!``]!``]!``]!!0```Q$`!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!````!!!!!!!!!!$`!!!!!!!!!!!!!!!!!!!!!0``!0]!!!!!!!!!!!!!!!!!!!!!!0]!!!!!!!!!!!!!``]!!0``!!!!``]!``]!`Q$``Q!!``]!!0]!!0``!!$``Q!!!!$`!0]!!!$`!!$`!0]!`Q$`!!$`!0]!`Q!!!0``!0```Q!!!0``!0]!!0]!``]!!!$`!0``!!$`!!!!``]!!!!!!!!!!!!!!!!!!!$`!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0```````````````````````````````````````````Q!!!0)!!5:13&amp;!!!!!"!!*52%.$!!!!!2&amp;$4&amp;.636"@17.D:8.T,G.U&lt;&amp;"53$!!!!"2!!!!"AI]=G6T&lt;X6S9W5_#5:S97VF&gt;W^S;QF1=G^W;72F=H-/4&amp;:$&lt;'&amp;T=URJ9H*B=HE-4G6X17.D:8.T&lt;X*T%5.-5V6*5&amp;^"9W.F=X-O9X2M!!!!!Q!!1A!!!!!!!!!"!!!!66"53$!!!!"7!!!!"AI]=G6T&lt;X6S9W5_#5:S97VF&gt;W^S;QF1=G^W;72F=H-/4&amp;:$&lt;'&amp;T=URJ9H*B=HE-4G6X17.D:8.T&lt;X*T&amp;EVF&lt;7*F=F:*1X*F982J&lt;WYO&lt;(:M;7)!!Q!!!!!(JQ!!&amp;S6YH+V9&lt;7R462B_4X?\X89&lt;OZXMIXSV+\?6#?6DIACC),NA.B"V9XR'I+Q&amp;BD#A[V3C1;*VOB`%%!+*,C4_YB=**B"-$$_)+=[E`C#9'!74#D_-/EDU"W)Q&gt;^@XH.0\W7Y&gt;-%B/SHA`H`&gt;ZHX-[A)9GK&gt;%V#E&gt;6).)^`,"/"7]]3Q!S52(S@S)$)(72`Y"-^2-66IJ&gt;UEX8+*GB1F5]'R%8+I0Q6^[;$-.0UCUU,:@]'-SL1EU]W_"LFX/3@(['0/D7I`JAJH3=D,IWSY(\YP(U15Q)[4HU^%8*+"#F32$3I&gt;&gt;CP9GU4(`KC9J_&amp;N+DAK2E:S&lt;FX*-9%6.`QU-/EUO9/B]3-/1=O($BAOHEYUY26M:T[)0WN.TB=8RKU;&gt;0TMVD0F\O-]R;:$Z+901EL:U[/6W@5,)3OK,@/TIUF`2=3E"&gt;7&gt;2PLQJ4Z&gt;QKU3`?CN^Q6\_?`AI)E01V\5`N:G;(#051*N`$(&gt;^[/APG59PD5(";Y76%[M2`&gt;[LQ6$LLWAE#T2K!Z?1(_*E.2.!(MJQ/:!U&lt;C%=:F-#=S-E3%V'%4'BG[`\_PF1C'4SY/^C^0^&lt;8&amp;TS5\(ELFEI%Y\&amp;5L("7,SB:`R++!UX';6)(!PE30L0C@B$/HDW,5/"JOL[)LAVSTP$T)998Y9S"@&gt;T%E'9V-6S"'#L`&lt;NB$=1QP=_HML7$MD&lt;,T.DMXM(-O/[_SMYO&gt;_US'NS$$@:+T&gt;"P$HZF]BD_,R$PK:0AJ-A2HRW(L%OZE9@A1)H9/4IXD]RT[P'^D_"$G/;@H'9`B3QM:0K4H-BB__P2J*]/8'1Q8#$%9@AM&amp;:Y?I`;.^45Z#.W.Y"@0IQ(&amp;A*EJ4UK&amp;&gt;)101R?$X[0!@,MLH:M4`W0DY"]O"TC!4KGF.*J$"]7"&lt;+H'A4?FTT/-V&amp;9\%M[)`K?1#K_4TM_4"R43(B$&lt;TI*E6YW;'P[LQ&lt;DQL_&amp;&amp;GP";_6W!JD2$7E&gt;&amp;]$*E($RYA-HDS;N*.=%S!N%R`YIGG5_(Z#X@H]V?J-!68I58/F&lt;'I#VG,5T$^$*C#E3Z@PIS2]01&amp;M"38Z#?D:=&amp;-A,.Q#JI%G5E40;61@KS\R"!.8WB+*^:%4\OJ*W"7UY\60#PH8*:K&gt;0^(L+:\YN6UW[J"(B\&amp;;N[WS%1:6#-QT;6F!HG`+LQ&amp;QFM"R;+(CU6::-"FBPY!1W_3=\/QU@RCU[(0BXFW\3KS("_C:Z^&gt;^BIA#APBB)-'281P&lt;3S`2V`_"DBG_H)_/R*_:#S`BS]`CH_UB-_!M@Q?PPTI9]NT&lt;[TW0M&lt;WOCS90Y(\NNB=`L%R`]3+_6[/O2!:+'/8V_MKG:N@&gt;1-N=\O]+FFE+KW_[NJ&gt;!'U51C7F.B-K\UQF?XLX/,;\5I8XM"N5J2I?6;4JX3$AVHKRC:^P`),&gt;^Q^]Q@AM5*,3&amp;O^`RQH,0A^&lt;0H^L_8T6QF1MPY6P=;7&amp;&amp;!,_L9/&gt;%^!'1&gt;='9QLE;==&amp;3@`Y)6ZS#G3R&gt;1J8Q__[_#$=/!CR-X+-F*_IL;.0$.E0XLL;?PVD@7W$`L%B(SS/6S;/,%D6O2H6O5.4W=D+&gt;(7/[_KMH!_UG`*=C?CWF*"H&amp;`!H2`F,X&gt;W*0K=K)ZE3FD;KV_:`PEG&amp;1VC2A',&gt;4H06M-PA"6:6B6Z6EF;VXE[EP\%I&amp;UQN331Z")Z350@\K/[X[\L0OX4$#HSRP?K9\@8LVS?I_^5KJ$BDKCS-=7.(^&lt;"U!F%.RG4GVQA&gt;C6D=P3H:EUK5&gt;3T9:'LZN/*;8A66J&lt;3=GER1S\FJ+3X(;K:&lt;\\EAI&gt;65I8`DIV&gt;4Z*Y&lt;KZJ&gt;TJPF&lt;=&gt;_%74).+33@&lt;]9^(;6?]?[8QG_8/6YMU3#%'H3)C(QD7!(Y"HB`V7",^4@0MW\&gt;_%4.3_#WIBW"7]&lt;W3[#1N(X$M'&amp;CE^QI7L8&lt;7SF4`&gt;V0&lt;O3M?329%&gt;C&gt;]%4F,C6&lt;/-C/&gt;?*7@D.Y%)[B]:[3K++"PB,XX)H!,3#&lt;,V!T-%R`/[95KF*.OB*"1:\XHZ`.C,V8Y3!A`C&amp;8[');*7&amp;8A[RC"$H,6\&amp;N\Z_S^R(S7K'[@&lt;X@*9#`$)$O-Y%W)U!&lt;ZP!AZ)#80V+\%X]1H3E.X;AJXO&amp;!VOE_7R/]YUM11V,-".GQRRIQ9ZGL=7I^$1P(*/\F6#$X+5G4@1UO,OPE/&lt;=.+Z25TR,GTZ_6/MKS^BDP:SLMYYP#EM+^ND29"VC5'S0?QJ,Y;;&amp;?TSG[?.(N499ZHT@9BEC\&lt;%&amp;FB&gt;1XN(D.)1BS%S;['GE5!KL+7J+VW^CFA]@V.JBB(?YW&gt;)B8?JC(2*\BA#;&amp;#NGLSU&amp;+O5VS[ZW?0CO?H"8"\&lt;41:C3O%(@W--IC2?V%:MEQN9R*8'O]8XUV0AL7U%PS/$'NM,PYNPM1BB%)6QZLB!OG%1BB$=?81BBOV5)NX.QP;91)KT'=`MCQHJ@OWO(N86-7+/F9.7P'J%^/9LCKDQMLIMG%^=VDY'L6A483BOONX6=ZS/OBQ(MO0Y_*KYL3_$+-07WJP&lt;4/\OX`U!BKH]],+L04S;K)Y_"[BULKNMYKF5-6=^[3`XCT7`\D6^-CZWSIIVH)+X'Y/_L5+?UVF[2WKB3C7XC;OG-;Z11&amp;?L&amp;-_+^T)`'L[YT[QT0^$5Y8H:2`DQA.PY0$A:W.!!!!!!%!!!!=!!!!!1!!!!!!!!!$!!"1E2)5!!!!!!!!Q!!!'5!!!"V?*RD9'!I&amp;*"A_M&gt;1^Z?"3?!LE#(^FY&amp;:U)`R.Q-$JZ`!93$.+#!*&amp;*&lt;^S]!OK!U7VD[CS]%!";JMD"S3()=&amp;/=!S(#U;$0```V=J9O4Y?O1;8.%2(TB4::9]BQ1!:1A:GA!!!!!!!!1!!!!(!!!&amp;/Q!!!!=!!!!;4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B6'&amp;C4X*E:8)!!!""&amp;Q#!!!!!!!)!"1!(!!!-!%!!!@````]!!!!"!!%!!!!(!!!!!!!!!!%!!!!#!!!!!Q!!!!1!!!!&amp;!!!!"A!!!!!!!!!&lt;4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B6'FN:8.U97VQ!!!!'2=!A!!!!!!"!!5!"Q!!!1!!R)M?]1!!!!!!!!!G4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B4'&amp;T&gt;%&amp;Q='RJ:725;7VF=X2B&lt;8!!!!!:&amp;Q#!!!!!!!%!"1!(!!!"!!$%CR\R!!!!!!!!!"J-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;5?8"F2'6T9Q!!!3Q8!)!!!!!!!1!)!$$`````!!%!!!!!!2!!!!!*!#"!=!!)!!!!61!!%ER71WRB=X.-;7*S98*Z)&amp;*F:A!!'%"Q!!A!!!!'!!!+1X2M)&amp;*F:GZV&lt;1!!&amp;%"Q!!A!!!!#!!!(5G6B:#"731!71(!!#!!!!!)!!!B8=GFU:3"731!!%E!B$5VB;W5A:(FO97VJ9T]!6!$R!!!!!!!!!!)7476N9G6S6EF$=G6B&gt;'FP&lt;CZM&gt;GRJ9B&amp;$4&amp;.636"@17.D:8.T,G.U&lt;!!D1"9!!Q23:7&amp;E"6&gt;S;82F!V)P6Q!!"E&amp;D9W6T=Q!!%%!Q`````Q:4&gt;(*J&lt;G=!!"R!1!!"`````Q!'$U.S:7&amp;U:71A382F&lt;5F%=Q!5!&amp;!!"Q!!!!%!!A!$!!1!"1!(!!%!#!!!!!!!!!!?4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B2':M&gt;%2B&gt;'&amp;4;8JF!!!!'2=!A!!!!!!"!!5!!Q!!!1!!!!!!&amp;Q!!!!!!!!!;4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B2':M&gt;%2B&gt;'%!!!%P&amp;Q#!!!!!!!E!)%"Q!!A!!!"6!!!34&amp;:$&lt;'&amp;T=URJ9H*B=HEA5G6G!!!91(!!#!!!!!9!!!J$&gt;'QA5G6G&lt;H6N!!!51(!!#!!!!!)!!!&gt;3:7&amp;E)&amp;:*!":!=!!)!!!!!A!!#&amp;&gt;S;82F)&amp;:*!!!31#%.47&amp;L:3"E?7ZB&lt;7FD0Q"5!0%!!!!!!!!!!B:.:7VC:8*735.S:7&amp;U;7^O,GRW&lt;'FC%5.-5V6*5&amp;^"9W.F=X-O9X2M!#.!&amp;A!$"&amp;*F971&amp;6X*J&gt;'5$5C^8!!!'17.D:8.T!!!11$$`````"F.U=GFO:Q!!(%"!!!(`````!!901X*F982F:#"*&gt;'6N352T!"1!5!!(!!!!!1!#!!-!"!!&amp;!!=!!1!)!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!)6^O;6^-98.U3WZP&gt;WZ0&gt;WZJ&lt;G&gt;-6E.M98.T1WRV=X2F=A!!!5I8!)!!!!!!!1!)!$$`````!!%!!!!!!3Y!!!!*!#"!=!!)!!!!61!!%ER71WRB=X.-;7*S98*Z)&amp;*F:A!!'%"Q!!A!!!!'!!!+1X2M)&amp;*F:GZV&lt;1!!&amp;%"Q!!A!!!!#!!!(5G6B:#"731!71(!!#!!!!!)!!!B8=GFU:3"731!!%E!B$5VB;W5A:(FO97VJ9T]!6!$R!!!!!!!!!!)7476N9G6S6EF$=G6B&gt;'FP&lt;CZM&gt;GRJ9B&amp;$4&amp;.636"@17.D:8.T,G.U&lt;!!D1"9!!Q23:7&amp;E"6&gt;S;82F!V)P6Q!!"E&amp;D9W6T=Q!!%%!Q`````Q:4&gt;(*J&lt;G=!!"R!1!!"`````Q!'$U.S:7&amp;U:71A382F&lt;5F%=Q!S1&amp;!!"Q!!!!%!!A!$!!1!"1!((%*B=W6"9W.F=X.P=F.D=GFQ&gt;'6S,GRW9WRB=X-!!!%!#!!!!!!!!!!!"!!-!"1!!!!%!!!".!!!!#A!!!!#!!!%!!!!!#%!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"\1!!!\&gt;YH*V347`41""^DG-\#:3WI;1JJ/W7N/87)J!Y=-(A#MF3+E5*49`AW/P+QIGD^;:K&lt;^T\N`B"^"@!\.JN53E@9E?7:G&gt;G:^Z\9Q$0]!'8+%[F&gt;=AH9SZ'PC&gt;Y)*.MOJ??JMFYW?M.D`T_R\&gt;BS0.],Z1JOGY,:H8!A]A[&amp;IHEZG$`',#,#I#Z-^3IYR(1\)W].-DT8D)7A4BH!RY$\4*P!QV0JCIYH5_!F4*?!2T6H)V]N'ZC.4V,"&gt;&amp;UNR9/A]_=2?@49*+%&lt;\$E0P^/RRZ+E5R0A)\LQF!2W)O;$Y_9,`H%0]BRA=OP&amp;RO+N`E&lt;TJVX1=Y,/JE9BC+:33YI&amp;3IO\4O43J&gt;8&lt;B]/$!*LIAK,"DDL8DL0+=_SG/H8&lt;#;35Y,$IE!'6%.6$53!]1VVOJDF&amp;3^R$X88A2GH*[CZ8]RM))M!TW+CW[@3"4T!IDG09NT(N?48PPW44`)2ICW]`J^VVV7L'L4!V0&amp;+W"9N5P&amp;&gt;2B-0S6&lt;Q#+OG0)O)1:]G,K&amp;.8ECT$,,.0`]T,WZ*^_1@J#.3FI*GU4P#^UFT@%_T'&gt;;R1&lt;#6&lt;:+/6V9P\&gt;@)49:J+X:AE'+XA@V^JV5I3![QCLMI%S+&amp;NE&amp;+LO%R/P1^22@&lt;Z1Q^A6BN5[S,(?XP;CS72F$Z!&lt;27PCI!!!!!!!"F!!%!!A!$!!1!!!")!!]%!!!!!!]!W!$6!!!!51!0"!!!!!!0!.A!V1!!!&amp;I!$Q1!!!!!$Q$9!.5!!!"DA!#%!)!!!!]!W!$6#&amp;.F:W^F)&amp;6*#&amp;.F:W^F)&amp;6*#&amp;.F:W^F)&amp;6*!4!!!!"35V*$$1I!!UR71U.-1F:8!!!@I!!!"(U!!!!A!!!@A!!!!!!!!!!!!!!!)!!!!$1!!!2E!!!!(%R*1EY!!!!!!!!"9%R75V)!!!!!!!!"&gt;&amp;*55U=!!!!!!!!"C%.$5V1!!!!!!!!"H%R*&gt;GE!!!!!!!!"M%.04F!!!!!!!!!"R&amp;2./$!!!!!"!!!"W%2'2&amp;-!!!!!!!!#!%R*:(-!!!!!!!!#&amp;&amp;:*1U1!!!!#!!!#+(:F=H-!!!!%!!!#:&amp;.$5V)!!!!!!!!#S%&gt;$5&amp;)!!!!!!!!#X%F$4UY!!!!!!!!#]'FD&lt;$1!!!!!!!!$"'FD&lt;$A!!!!!!!!$'%R*:H!!!!!!!!!$,%:13')!!!!!!!!$1%:15U5!!!!!!!!$6&amp;:12&amp;!!!!!!!!!$;%R*9G1!!!!!!!!$@%*%3')!!!!!!!!$E%*%5U5!!!!!!!!$J&amp;:*6&amp;-!!!!!!!!$O%253&amp;!!!!!!!!!$T%V6351!!!!!!!!$Y%B*5V1!!!!!!!!$^&amp;:$6&amp;!!!!!!!!!%#%:515)!!!!!!!!%(!!!!!$`````!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0!!!!!!!!!!!`````Q!!!!!!!!$A!!!!!!!!!!$`````!!!!!!!!!01!!!!!!!!!!0````]!!!!!!!!!`!!!!!!!!!!!`````Q!!!!!!!!'I!!!!!!!!!!$`````!!!!!!!!!&lt;!!!!!!!!!!!P````]!!!!!!!!"X!!!!!!!!!!!`````Q!!!!!!!!(U!!!!!!!!!!$`````!!!!!!!!!E1!!!!!!!!!!0````]!!!!!!!!#6!!!!!!!!!!"`````Q!!!!!!!!1)!!!!!!!!!!,`````!!!!!!!!"=A!!!!!!!!!"0````]!!!!!!!!(7!!!!!!!!!!(`````Q!!!!!!!!&gt;M!!!!!!!!!!D`````!!!!!!!!"XQ!!!!!!!!!#@````]!!!!!!!!(E!!!!!!!!!!+`````Q!!!!!!!!?A!!!!!!!!!!$`````!!!!!!!!"\1!!!!!!!!!!0````]!!!!!!!!(T!!!!!!!!!!!`````Q!!!!!!!!@A!!!!!!!!!!$`````!!!!!!!!#'1!!!!!!!!!!0````]!!!!!!!!+;!!!!!!!!!!!`````Q!!!!!!!!ZM!!!!!!!!!!$`````!!!!!!!!$W1!!!!!!!!!!0````]!!!!!!!!8%!!!!!!!!!!!`````Q!!!!!!!"=9!!!!!!!!!!$`````!!!!!!!!&amp;S!!!!!!!!!!!0````]!!!!!!!!8-!!!!!!!!!!!`````Q!!!!!!!"?=!!!!!!!!!!$`````!!!!!!!!&amp;[1!!!!!!!!!!0````]!!!!!!!!=Z!!!!!!!!!!!`````Q!!!!!!!"TM!!!!!!!!!!$`````!!!!!!!!(01!!!!!!!!!!0````]!!!!!!!!&gt;)!!!!!!!!!#!`````Q!!!!!!!"]5!!!!!"B#98.F17.D:8.T&lt;X*49X*J=(2F=CZD&gt;'Q!!!!!</Property>
	<Property Name="NI.LVClass.Geneology" Type="Xml"><String>

<Name></Name>

<Val>!!!!!B:.:7VC:8*735.S:7&amp;U;7^O,GRW&lt;'FC(%*B=W6"9W.F=X.P=F.D=GFQ&gt;'6S,GRW9WRB=X.16%AQ!!!!!!!!!!!!!!!%!!%!!!!!!!!"!!!!!1!'!&amp;!!!!!"!!!!!!!!!!!!!!%/4'&amp;C6EF&amp;6S"09GJF9X1!5&amp;2)-!!!!!!!!!!!!!ABA!!!!!!!!!!!!!!!!!!"!!!!!!!"!1!!!!=!)%"Q!!A!!!"6!!!34&amp;:$&lt;'&amp;T=URJ9H*B=HEA5G6G!!!91(!!#!!!!!9!!!J$&gt;'QA5G6G&lt;H6N!!!51(!!#!!!!!)!!!&gt;3:7&amp;E)&amp;:*!":!=!!)!!!!!A!!#&amp;&gt;S;82F)&amp;:*!!!31#%.47&amp;L:3"E?7ZB&lt;7FD0Q"5!0%!!!!!!!!!!B:"9W.F=X.P=E.S:7&amp;U;7^O,GRW&lt;'FC%5.-5V6*5&amp;^"9W.F=X-O9X2M!#.!&amp;A!$"&amp;*F971&amp;6X*J&gt;'5$5C^8!!!'17.D:8.T!!#*!0("@EV"!!!!!R:"9W.F=X.P=E.S:7&amp;U;7^O,GRW&lt;'FC(%*B=W6"9W.F=X.P=F.D=GFQ&gt;'6S,GRW9WRB=X-91G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)O9X2M!$2!5!!'!!!!!1!#!!-!"!!&amp;(5.M&gt;8.U:8)A&lt;W9A9WRB=X-A=(*J&gt;G&amp;U:3"E982B!!%!"A!!!!&lt;```````````````````````````````]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1Z-97*73568)%^C;G6D&gt;!"16%AQ!!!!!!!!!!!!##'!!!!!!!!!!!!!!!!!!!%!!!!!!!)"!!!!#1!A1(!!#!!!!&amp;5!!"*-6E.M98.T4'FC=G&amp;S?3"3:79!!"B!=!!)!!!!"A!!#E.U&lt;#"3:7:O&gt;7U!!"2!=!!)!!!!!A!!"V*F971A6EE!&amp;E"Q!!A!!!!#!!!)6X*J&gt;'5A6EE!!"*!)1V.97NF)'2Z&lt;G&amp;N;7-`!&amp;1!]1!!!!!!!!!#&amp;E&amp;D9W6T=W^S1X*F982J&lt;WYO&lt;(:M;7)21UR465F18U&amp;D9W6T=SZD&gt;'Q!)U!7!!-%5G6B:!68=GFU:1.3,V=!!!:"9W.F=X-!!""!-0````]'5X2S;7ZH!!!=1%!!!@````]!"A^$=G6B&gt;'6E)%FU:7V*2(-!CQ$RQ:9\^!!!!!-717.D:8.T&lt;X*$=G6B&gt;'FP&lt;CZM&gt;GRJ9BR#98.F17.D:8.T&lt;X*49X*J=(2F=CZM&gt;G.M98.T'%*B=W6"9W.F=X.P=F.D=GFQ&gt;'6S,G.U&lt;!!W1&amp;!!"Q!!!!%!!A!$!!1!"1!((5.M&gt;8.U:8)A&lt;W9A9WRB=X-A=(*J&gt;G&amp;U:3"E982B!!%!#!!!!!=!!!!!!!!!!1!!!!)!!!!$!!!!"!!!!!8`````!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1Z-97*73568)%^C;G6D&gt;!"16%AQ!!!!!!!!!!!!##'!!!!!!!!!!!!!!!!!!!%!!!!!!!!!!!!!#1!A1(!!#!!!!&amp;5!!"*-6E.M98.T4'FC=G&amp;S?3"3:79!!"B!=!!)!!!!"A!!#E.U&lt;#"3:7:O&gt;7U!!"2!=!!)!!!!!A!!"V*F971A6EE!&amp;E"Q!!A!!!!#!!!)6X*J&gt;'5A6EE!!"*!)1V.97NF)'2Z&lt;G&amp;N;7-`!&amp;1!]1!!!!!!!!!#&amp;EVF&lt;7*F=F:*1X*F982J&lt;WYO&lt;(:M;7)21UR465F18U&amp;D9W6T=SZD&gt;'Q!)U!7!!-%5G6B:!68=GFU:1.3,V=!!!:"9W.F=X-!!""!-0````]'5X2S;7ZH!!!=1%!!!@````]!"A^$=G6B&gt;'6E)%FU:7V*2(-!CQ$RR)M?]1!!!!-7476N9G6S6EF$=G6B&gt;'FP&lt;CZM&gt;GRJ9BR#98.F17.D:8.T&lt;X*49X*J=(2F=CZM&gt;G.M98.T'%*B=W6"9W.F=X.P=F.D=GFQ&gt;'6S,G.U&lt;!!W1&amp;!!"Q!!!!%!!A!$!!1!"1!((5.M&gt;8.U:8)A&lt;W9A9WRB=X-A=(*J&gt;G&amp;U:3"E982B!!%!#!!!!!(````_!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1Z-97*73568)%^C;G6D&gt;!"16%AQ!!!!!!!!!!!!#&amp;#!!A!!!!!!!!!!!!%!!!!T17.D:8.T&lt;X*$=G6B&gt;'FP&lt;CZM&gt;GRJ9DJ#98.F17.D:8.T&lt;X*49X*J=(2F=CZM&gt;G.M98.T</Val>

</String>

</Property>
	<Property Name="NI.LVClass.IsTransferClass" Type="Bool">false</Property>
	<Property Name="NI.LVClass.LowestCompatibleVersion" Type="Str">1.0.0.0</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Item Name="BaseAccessorScripter.ctl" Type="Class Private Data" URL="BaseAccessorScripter.ctl">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="Public" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="Can Script Property Accessor.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/BaseAccessorScripter/Can Script Property Accessor.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'.!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#*!)2R$97YA5W.S;8"U)&amp;"S&lt;X"F=H2Z)%&amp;D9W6T=W^S!!"91(!!(A!!.2:.:7VC:8*735.S:7&amp;U;7^O,GRW&lt;'FC(%*B=W6"9W.F=X.P=F.D=GFQ&gt;'6S,GRW9WRB=X-!'%*B=W6"9W.F=X.P=F.D=GFQ&gt;'6S)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"71(!!(A!!.2:.:7VC:8*735.S:7&amp;U;7^O,GRW&lt;'FC(%*B=W6"9W.F=X.P=F.D=GFQ&gt;'6S,GRW9WRB=X-!&amp;U*B=W6"9W.F=X.P=F.D=GFQ&gt;'6S)'FO!&amp;1!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!"!!)!A!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!!!!!#1!!!!!!%!#1!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342710272</Property>
		</Item>
		<Item Name="GetVIRefs.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/BaseAccessorScripter/GetVIRefs.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'6!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!&amp;E"Q!!A!!!!#!!!)6X*J&gt;'5A6EE!!"2!=!!)!!!!!A!!"V*F971A6EE!7%"Q!"Y!!$57476N9G6S6EF$=G6B&gt;'FP&lt;CZM&gt;GRJ9BR#98.F17.D:8.T&lt;X*49X*J=(2F=CZM&gt;G.M98.T!"B#98.F17.D:8.T&lt;X*49X*J=(2F=C"P&gt;81!!!1!!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!&amp;:!=!!?!!!V&amp;EVF&lt;7*F=F:*1X*F982J&lt;WYO&lt;(:M;7)=1G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)O&lt;(:D&lt;'&amp;T=Q!81G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)A;7Y!6!$Q!!Q!!Q!%!!5!"A!(!!=!"Q!(!!A!"Q!(!!E#!!"Y!!!.#!!!#1!!!!E!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!*!!!!!!!1!+!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">8192</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1117782528</Property>
		</Item>
		<Item Name="ScriptAccessorVIs.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/BaseAccessorScripter/ScriptAccessorVIs.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)T!!!!$A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;B!=!!?!!!V&amp;EVF&lt;7*F=F:*1X*F982J&lt;WYO&lt;(:M;7)=1G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)O&lt;(:D&lt;'&amp;T=Q!91G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)A&lt;X6U!!!=1$$`````%V:J=H2V97QA2G^M:'6S)%ZB&lt;75!'E!B&amp;%.S:7&amp;U:3"B=S"1=G^Q:8*U;76T!!!=1#%837ZD&lt;(6E:3"F=H*P=C"U:8*N;7ZB&lt;(-!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!C1#%=47&amp;L:3"$&lt;'&amp;T=S"5:8*N;7ZB&lt;(-A2(FO97VJ9Q!!6!$R!!!!!!!!!!)7476N9G6S6EF$=G6B&gt;'FP&lt;CZM&gt;GRJ9B&amp;$4&amp;.636"@17.D:8.T,G.U&lt;!!D1"9!!Q23:7&amp;E"6&gt;S;82F!V)P6Q!!"E&amp;D9W6T=Q!!6E"Q!"Y!!$57476N9G6S6EF$=G6B&gt;'FP&lt;CZM&gt;GRJ9BR#98.F17.D:8.T&lt;X*49X*J=(2F=CZM&gt;G.M98.T!"&gt;#98.F17.D:8.T&lt;X*49X*J=(2F=C"J&lt;A"5!0!!$!!$!!1!"!!&amp;!!1!"A!(!!A!#1!+!!M!$!-!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!"%A!!!")!!!!3!!!!#A!!!!A!!!!)!!!!EA!!!!!"!!U!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1117782544</Property>
		</Item>
		<Item Name="CreateBaseScripter.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/BaseAccessorScripter/CreateBaseScripter.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#K!!!!"!"-1(!!(A!!.2:.:7VC:8*735.S:7&amp;U;7^O,GRW&lt;'FC(%*B=W6"9W.F=X.P=F.D=GFQ&gt;'6S,GRW9WRB=X-!$&amp;.D=GFQ&gt;'6S)%^V&gt;!!!'%"Q!!A!!!!'!!!+1X2M)&amp;*F:GZV&lt;1!!)%"Q!!A!!!"6!!!34&amp;:$&lt;'&amp;T=URJ9H*B=HEA5G6G!!!?!0!!!Q!!!!%!!A)!!"!!!!E!!!!)!!!!#!!!!!!"!!-!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">8192</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1117782528</Property>
		</Item>
		<Item Name="GetVIItemIDs.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/BaseAccessorScripter/GetVIItemIDs.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'A!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!""!-0````]'5X2S;7ZH!!!91%!!!@````]!"1J733"*&gt;'6N352T!!"91(!!(A!!.2:.:7VC:8*735.S:7&amp;U;7^O,GRW&lt;'FC(%*B=W6"9W.F=X.P=F.D=GFQ&gt;'6S,GRW9WRB=X-!'%*B=W6"9W.F=X.P=F.D=GFQ&gt;'6S)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"71(!!(A!!.2:.:7VC:8*735.S:7&amp;U;7^O,GRW&lt;'FC(%*B=W6"9W.F=X.P=F.D=GFQ&gt;'6S,GRW9WRB=X-!&amp;U*B=W6"9W.F=X.P=F.D=GFQ&gt;'6S)'FO!'%!]!!-!!-!"!!'!!=!"!!%!!1!"!!)!!1!"!!*!A!!?!!!$1A!!!!!!!!*!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!!!!!!1!!!.!!!!$!!!!!!!!!!!!!!"!!I!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">8192</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1117782528</Property>
		</Item>
	</Item>
	<Item Name="Protected" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
		<Item Name="Accessors" Type="Folder">
			<Item Name="GetReadVI.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/BaseAccessorScripter/GetReadVI.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;`!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"2!=!!)!!!!!A!!"V*F971A6EE!7%"Q!"Y!!$57476N9G6S6EF$=G6B&gt;'FP&lt;CZM&gt;GRJ9BR#98.F17.D:8.T&lt;X*49X*J=(2F=CZM&gt;G.M98.T!"B#98.F17.D:8.T&lt;X*49X*J=(2F=C"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!6E"Q!"Y!!$57476N9G6S6EF$=G6B&gt;'FP&lt;CZM&gt;GRJ9BR#98.F17.D:8.T&lt;X*49X*J=(2F=CZM&gt;G.M98.T!"&gt;#98.F17.D:8.T&lt;X*49X*J=(2F=C"J&lt;A"5!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!)!!(A!!!U)!!!!!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!E!!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">8192</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1117782528</Property>
			</Item>
			<Item Name="GetWriteVI.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/BaseAccessorScripter/GetWriteVI.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'"!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!":!=!!)!!!!!A!!#&amp;&gt;S;82F)&amp;:*!!"91(!!(A!!.2:.:7VC:8*735.S:7&amp;U;7^O,GRW&lt;'FC(%*B=W6"9W.F=X.P=F.D=GFQ&gt;'6S,GRW9WRB=X-!'%*B=W6"9W.F=X.P=F.D=GFQ&gt;'6S)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"71(!!(A!!.2:.:7VC:8*735.S:7&amp;U;7^O,GRW&lt;'FC(%*B=W6"9W.F=X.P=F.D=GFQ&gt;'6S,GRW9WRB=X-!&amp;U*B=W6"9W.F=X.P=F.D=GFQ&gt;'6S)'FO!&amp;1!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!"!!)!A!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!!!!!#1!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">8192</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1117782528</Property>
			</Item>
			<Item Name="WriteAccess.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/BaseAccessorScripter/WriteAccess.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'`!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;B!=!!?!!!V&amp;EVF&lt;7*F=F:*1X*F982J&lt;WYO&lt;(:M;7)=1G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)O&lt;(:D&lt;'&amp;T=Q!91G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!&amp;1!]1!!!!!!!!!#&amp;EVF&lt;7*F=F:*1X*F982J&lt;WYO&lt;(:M;7)21UR465F18U&amp;D9W6T=SZD&gt;'Q!)U!7!!-%5G6B:!68=GFU:1.3,V=!!!:"9W.F=X-!!&amp;:!=!!?!!!V&amp;EVF&lt;7*F=F:*1X*F982J&lt;WYO&lt;(:M;7)=1G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)O&lt;(:D&lt;'&amp;T=Q!81G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)A;7Y!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!(!!A#!!"Y!!!.#!!!!!!!!!!!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!#!!!!*)!!!!!!1!*!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">8192</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1117782528</Property>
			</Item>
			<Item Name="WriteMakeDynamic.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/BaseAccessorScripter/WriteMakeDynamic.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;^!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;B!=!!?!!!V&amp;EVF&lt;7*F=F:*1X*F982J&lt;WYO&lt;(:M;7)=1G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)O&lt;(:D&lt;'&amp;T=Q!91G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!"*!)1V.97NF)'2Z&lt;G&amp;N;7-`!&amp;:!=!!?!!!V&amp;EVF&lt;7*F=F:*1X*F982J&lt;WYO&lt;(:M;7)=1G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)O&lt;(:D&lt;'&amp;T=Q!81G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)A;7Y!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!(!!A#!!"Y!!!.#!!!!!!!!!!!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!#!!!!*)!!!!!!1!*!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">8192</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1117782528</Property>
			</Item>
			<Item Name="WriteWriteVI.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/BaseAccessorScripter/WriteWriteVI.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'"!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;B!=!!?!!!V&amp;EVF&lt;7*F=F:*1X*F982J&lt;WYO&lt;(:M;7)=1G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)O&lt;(:D&lt;'&amp;T=Q!91G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!":!=!!)!!!!!A!!#&amp;&gt;S;82F)&amp;:*!!"71(!!(A!!.2:.:7VC:8*735.S:7&amp;U;7^O,GRW&lt;'FC(%*B=W6"9W.F=X.P=F.D=GFQ&gt;'6S,GRW9WRB=X-!&amp;U*B=W6"9W.F=X.P=F.D=GFQ&gt;'6S)'FO!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"Q!)!A!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!A!!!#3!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">8192</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1117782528</Property>
			</Item>
			<Item Name="WriteReadVI.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/BaseAccessorScripter/WriteReadVI.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;`!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;B!=!!?!!!V&amp;EVF&lt;7*F=F:*1X*F982J&lt;WYO&lt;(:M;7)=1G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)O&lt;(:D&lt;'&amp;T=Q!91G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!"2!=!!)!!!!!A!!"V*F971A6EE!6E"Q!"Y!!$57476N9G6S6EF$=G6B&gt;'FP&lt;CZM&gt;GRJ9BR#98.F17.D:8.T&lt;X*49X*J=(2F=CZM&gt;G.M98.T!"&gt;#98.F17.D:8.T&lt;X*49X*J=(2F=C"J&lt;A"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!)!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!)!!!!EA!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">8192</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1117782528</Property>
			</Item>
			<Item Name="WriteCtrlRef.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/BaseAccessorScripter/WriteCtrlRef.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'$!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;B!=!!?!!!V&amp;EVF&lt;7*F=F:*1X*F982J&lt;WYO&lt;(:M;7)=1G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)O&lt;(:D&lt;'&amp;T=Q!91G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!"B!=!!)!!!!"A!!#E.U&lt;#"3:7:O&gt;7U!!&amp;:!=!!?!!!V&amp;EVF&lt;7*F=F:*1X*F982J&lt;WYO&lt;(:M;7)=1G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)O&lt;(:D&lt;'&amp;T=Q!81G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)A;7Y!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!(!!A#!!"Y!!!.#!!!!!!!!!!!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!#!!!!*)!!!!!!1!*!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">8192</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1117782528</Property>
			</Item>
			<Item Name="WriteClassRef.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/BaseAccessorScripter/WriteClassRef.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!',!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;B!=!!?!!!V&amp;EVF&lt;7*F=F:*1X*F982J&lt;WYO&lt;(:M;7)=1G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)O&lt;(:D&lt;'&amp;T=Q!91G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!#"!=!!)!!!!61!!%ER71WRB=X.-;7*S98*Z)&amp;*F:A!!6E"Q!"Y!!$57476N9G6S6EF$=G6B&gt;'FP&lt;CZM&gt;GRJ9BR#98.F17.D:8.T&lt;X*49X*J=(2F=CZM&gt;G.M98.T!"&gt;#98.F17.D:8.T&lt;X*49X*J=(2F=C"J&lt;A"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!)!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!)!!!!EA!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">8192</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1117782528</Property>
			</Item>
			<Item Name="GetCtrlRefnum.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/BaseAccessorScripter/GetCtrlRefnum.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'$!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"B!=!!)!!!!"A!!#E.U&lt;#"3:7:O&gt;7U!!&amp;B!=!!?!!!V&amp;EVF&lt;7*F=F:*1X*F982J&lt;WYO&lt;(:M;7)=1G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)O&lt;(:D&lt;'&amp;T=Q!91G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!&amp;:!=!!?!!!V&amp;EVF&lt;7*F=F:*1X*F982J&lt;WYO&lt;(:M;7)=1G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)O&lt;(:D&lt;'&amp;T=Q!81G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)A;7Y!6!$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!"!!%!!A#!!"Y!!!.#!!!!!!!!!E!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!*!!!!!!!1!*!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">8192</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1117782528</Property>
			</Item>
		</Item>
		<Item Name="GetControlTermIdx.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/BaseAccessorScripter/GetControlTermIdx.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%&lt;!!!!"1!%!!!!$5!(!!&gt;5:8*N372Y!&amp;B!=!!?!!!V&amp;EVF&lt;7*F=F:*1X*F982J&lt;WYO&lt;(:M;7)=1G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)O&lt;(:D&lt;'&amp;T=Q!91G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)A&lt;X6U!!"71(!!(A!!.2:.:7VC:8*735.S:7&amp;U;7^O,GRW&lt;'FC(%*B=W6"9W.F=X.P=F.D=GFQ&gt;'6S,GRW9WRB=X-!&amp;U*B=W6"9W.F=X.P=F.D=GFQ&gt;'6S)'FO!&amp;1!]!!-!!!!!!!"!!)!!!!!!!!!!!!!!!!!!!!$!A!!?!!!!!!!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!#1!!!!!!%!"!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">8192</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1117782528</Property>
		</Item>
		<Item Name="GetIndicatorTermIdx.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/BaseAccessorScripter/GetIndicatorTermIdx.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%&lt;!!!!"1!%!!!!$5!(!!&gt;5:8*N372Y!&amp;B!=!!?!!!V&amp;EVF&lt;7*F=F:*1X*F982J&lt;WYO&lt;(:M;7)=1G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)O&lt;(:D&lt;'&amp;T=Q!91G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)A&lt;X6U!!"71(!!(A!!.2:.:7VC:8*735.S:7&amp;U;7^O,GRW&lt;'FC(%*B=W6"9W.F=X.P=F.D=GFQ&gt;'6S,GRW9WRB=X-!&amp;U*B=W6"9W.F=X.P=F.D=GFQ&gt;'6S)'FO!&amp;1!]!!-!!!!!!!"!!)!!!!!!!!!!!!!!!!!!!!$!A!!?!!!!!!!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!#1!!!!!!%!"!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">8192</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1117782528</Property>
		</Item>
		<Item Name="CreateControl.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/BaseAccessorScripter/CreateControl.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'(!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"R!=!!)!!!!"A!!$UZF&gt;S"$&lt;WZU=G^M)&amp;*F:A"91(!!(A!!.2:.:7VC:8*735.S:7&amp;U;7^O,GRW&lt;'FC(%*B=W6"9W.F=X.P=F.D=GFQ&gt;'6S,GRW9WRB=X-!'%*B=W6"9W.F=X.P=F.D=GFQ&gt;'6S)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"71(!!(A!!.2:.:7VC:8*735.S:7&amp;U;7^O,GRW&lt;'FC(%*B=W6"9W.F=X.P=F.D=GFQ&gt;'6S,GRW9WRB=X-!&amp;U*B=W6"9W.F=X.P=F.D=GFQ&gt;'6S)'FO!&amp;1!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!"!!)!Q!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!#1!!!!!!%!#1!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082143248</Property>
		</Item>
		<Item Name="CreateControlFromReference.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/BaseAccessorScripter/CreateControlFromReference.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!$Z!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!(%"Q!!A!!!!'!!!/4G6X)%^C;G6D&gt;#"3:79!!"J!=!!)!!!!!A!!$6:*)&amp;*F:GZV&lt;3"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!91(!!#!!!!!9!!!J4=G-A1X2M5G6G!!!71(!!#!!!!!)!!!F733"3:7:O&gt;7U!-!$Q!!9!!Q!%!!5!"A!(!!A$!!"1!!!.!Q!!#1!!!!U&amp;!!!+!!!!#A!!!!I!!!!!!1!*!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">8192</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1117782544</Property>
		</Item>
		<Item Name="CreateIndicator.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/BaseAccessorScripter/CreateIndicator.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'(!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"R!=!!)!!!!"A!!$UZF&gt;S"$&lt;WZU=G^M)&amp;*F:A"91(!!(A!!.2:.:7VC:8*735.S:7&amp;U;7^O,GRW&lt;'FC(%*B=W6"9W.F=X.P=F.D=GFQ&gt;'6S,GRW9WRB=X-!'%*B=W6"9W.F=X.P=F.D=GFQ&gt;'6S)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"71(!!(A!!.2:.:7VC:8*735.S:7&amp;U;7^O,GRW&lt;'FC(%*B=W6"9W.F=X.P=F.D=GFQ&gt;'6S,GRW9WRB=X-!&amp;U*B=W6"9W.F=X.P=F.D=GFQ&gt;'6S)'FO!&amp;1!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!"!!)!Q!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!#1!!!!!!%!#1!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082143248</Property>
		</Item>
		<Item Name="GetControlLabel.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/BaseAccessorScripter/GetControlLabel.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#L!!!!#!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!&amp;%!Q`````QJ-97*F&lt;#Z5:8BU!!!71&amp;!!!Q!!!!%!!ABF=H*P=C"*4Q!!'%"Q!!A!!!!'!!!+1X2M)&amp;*F:GZV&lt;1!!*!$Q!!1!!Q!%!!5!"A-!!#A!!!U#!!!*!!!!#A!!!!A!!!!!!1!(!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">8192</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1117782544</Property>
		</Item>
		<Item Name="GetElementNames.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/BaseAccessorScripter/GetElementNames.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'6!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!""!-0````]'5X2S;7ZH!!!;1%!!!@````]!"1V&amp;&lt;'6N:7ZU)%ZB&lt;76T!&amp;B!=!!?!!!V&amp;EVF&lt;7*F=F:*1X*F982J&lt;WYO&lt;(:M;7)=1G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)O&lt;(:D&lt;'&amp;T=Q!91G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!&amp;:!=!!?!!!V&amp;EVF&lt;7*F=F:*1X*F982J&lt;WYO&lt;(:M;7)=1G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)O&lt;(:D&lt;'&amp;T=Q!81G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)A;7Y!6!$Q!!Q!!Q!%!!9!"Q!%!!1!"!!%!!A!"!!%!!E$!!"Y!!!.#!!!!!!!!!E!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!*!!!!!!!1!+!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">8192</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1117782544</Property>
		</Item>
		<Item Name="GetMemberName.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/BaseAccessorScripter/GetMemberName.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;`!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"2!-0````]+476N9G6S4G&amp;N:1!!7%"Q!"Y!!$57476N9G6S6EF$=G6B&gt;'FP&lt;CZM&gt;GRJ9BR#98.F17.D:8.T&lt;X*49X*J=(2F=CZM&gt;G.M98.T!"B#98.F17.D:8.T&lt;X*49X*J=(2F=C"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!6E"Q!"Y!!$57476N9G6S6EF$=G6B&gt;'FP&lt;CZM&gt;GRJ9BR#98.F17.D:8.T&lt;X*49X*J=(2F=CZM&gt;G.M98.T!"&gt;#98.F17.D:8.T&lt;X*49X*J=(2F=C"J&lt;A"5!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!-!!(A!!!U)!!!!!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!E!!!!!!"!!E!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">8192</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1117782544</Property>
		</Item>
		<Item Name="GetReadVITemplatePath.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/BaseAccessorScripter/GetReadVITemplatePath.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'8!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!""!-P````](6EEA5'&amp;U;!"91(!!(A!!.2:.:7VC:8*735.S:7&amp;U;7^O,GRW&lt;'FC(%*B=W6"9W.F=X.P=F.D=GFQ&gt;'6S,GRW9WRB=X-!'%*B=W6"9W.F=X.P=F.D=GFQ&gt;'6S)'^V&gt;!!!(%!B&amp;UFO9WRV:'5A:8*S&lt;X)A&gt;'6S&lt;7FO97RT!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!6E"Q!"Y!!$57476N9G6S6EF$=G6B&gt;'FP&lt;CZM&gt;GRJ9BR#98.F17.D:8.T&lt;X*49X*J=(2F=CZM&gt;G.M98.T!"&gt;#98.F17.D:8.T&lt;X*49X*J=(2F=C"J&lt;A"5!0!!$!!$!!1!"1!'!!1!"!!%!!=!#!!%!!1!#1-!!(A!!!U)!!!!!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!3!!!!#A!!!!!!!!!!!!!!E!!!!!!"!!I!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">8192</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1117782544</Property>
		</Item>
		<Item Name="GetWriteVITemplatePath.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/BaseAccessorScripter/GetWriteVITemplatePath.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'8!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!""!-P````](6EEA5'&amp;U;!"91(!!(A!!.2:.:7VC:8*735.S:7&amp;U;7^O,GRW&lt;'FC(%*B=W6"9W.F=X.P=F.D=GFQ&gt;'6S,GRW9WRB=X-!'%*B=W6"9W.F=X.P=F.D=GFQ&gt;'6S)'^V&gt;!!!(%!B&amp;UFO9WRV:'5A:8*S&lt;X)A&gt;'6S&lt;7FO97RT!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!6E"Q!"Y!!$57476N9G6S6EF$=G6B&gt;'FP&lt;CZM&gt;GRJ9BR#98.F17.D:8.T&lt;X*49X*J=(2F=CZM&gt;G.M98.T!"&gt;#98.F17.D:8.T&lt;X*49X*J=(2F=C"J&lt;A"5!0!!$!!$!!1!"1!'!!1!"!!%!!=!#!!%!!1!#1-!!(A!!!U)!!!!!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!3!!!!#A!!!!!!!!!!!!!!E!!!!!!"!!I!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">8192</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1117782544</Property>
		</Item>
		<Item Name="SelectElementToRead.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/BaseAccessorScripter/SelectElementToRead.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;L!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;B!=!!?!!!V&amp;EVF&lt;7*F=F:*1X*F982J&lt;WYO&lt;(:M;7)=1G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)O&lt;(:D&lt;'&amp;T=Q!91G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!&amp;:!=!!?!!!V&amp;EVF&lt;7*F=F:*1X*F982J&lt;WYO&lt;(:M;7)=1G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)O&lt;(:D&lt;'&amp;T=Q!81G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)A;7Y!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=$!!"Y!!!.#!!!!!!!!!!!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!*)!!!!!!1!)!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">8192</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1117782544</Property>
		</Item>
		<Item Name="SelectElementToWrite.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/BaseAccessorScripter/SelectElementToWrite.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;L!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;B!=!!?!!!V&amp;EVF&lt;7*F=F:*1X*F982J&lt;WYO&lt;(:M;7)=1G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)O&lt;(:D&lt;'&amp;T=Q!91G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!&amp;:!=!!?!!!V&amp;EVF&lt;7*F=F:*1X*F982J&lt;WYO&lt;(:M;7)=1G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)O&lt;(:D&lt;'&amp;T=Q!81G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)A;7Y!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=$!!"Y!!!.#!!!!!!!!!!!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!*)!!!!!!1!)!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">8192</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1117782544</Property>
		</Item>
		<Item Name="WireUpReadVI.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/BaseAccessorScripter/WireUpReadVI.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'J!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#*!=!!)!!"!&amp;A!!&amp;%VV&lt;(2J:H*B&lt;75A5X2V9X1A&lt;X6U!!"91(!!(A!!.2:.:7VC:8*735.S:7&amp;U;7^O,GRW&lt;'FC(%*B=W6"9W.F=X.P=F.D=GFQ&gt;'6S,GRW9WRB=X-!'%*B=W6"9W.F=X.P=F.D=GFQ&gt;'6S)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!=1(!!#!!!!!9!!!^/:8=A1W^O&gt;(*P&lt;#"3:79!6E"Q!"Y!!$57476N9G6S6EF$=G6B&gt;'FP&lt;CZM&gt;GRJ9BR#98.F17.D:8.T&lt;X*49X*J=(2F=CZM&gt;G.M98.T!"&gt;#98.F17.D:8.T&lt;X*49X*J=(2F=C"J&lt;A"5!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!A!#1-!!(A!!!U)!!!!!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!)!!!!E!!!!!!"!!I!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">8192</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1117782544</Property>
		</Item>
		<Item Name="WireUpWriteVI.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/BaseAccessorScripter/WireUpWriteVI.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'J!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#*!=!!)!!"!&amp;A!!&amp;%VV&lt;(2J:H*B&lt;75A5X2V9X1A&lt;X6U!!"91(!!(A!!.2:.:7VC:8*735.S:7&amp;U;7^O,GRW&lt;'FC(%*B=W6"9W.F=X.P=F.D=GFQ&gt;'6S,GRW9WRB=X-!'%*B=W6"9W.F=X.P=F.D=GFQ&gt;'6S)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!=1(!!#!!!!!9!!!^/:8=A1W^O&gt;(*P&lt;#"3:79!6E"Q!"Y!!$57476N9G6S6EF$=G6B&gt;'FP&lt;CZM&gt;GRJ9BR#98.F17.D:8.T&lt;X*49X*J=(2F=CZM&gt;G.M98.T!"&gt;#98.F17.D:8.T&lt;X*49X*J=(2F=C"J&lt;A"5!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!A!#1-!!(A!!!U)!!!!!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!)!!!!E!!!!!!"!!I!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">8192</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1117782544</Property>
		</Item>
		<Item Name="CallUserScripting.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/BaseAccessorScripter/CallUserScripting.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;B!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;B!=!!?!!!V&amp;EVF&lt;7*F=F:*1X*F982J&lt;WYO&lt;(:M;7)=1G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)O&lt;(:D&lt;'&amp;T=Q!91G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)A&lt;X6U!!!71&amp;!!!Q!!!!%!!ABF=H*P=C"J&lt;A!!6E"Q!"Y!!$57476N9G6S6EF$=G6B&gt;'FP&lt;CZM&gt;GRJ9BR#98.F17.D:8.T&lt;X*49X*J=(2F=CZM&gt;G.M98.T!"&gt;#98.F17.D:8.T&lt;X*49X*J=(2F=C"J&lt;A"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q)!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!E!!!!!!"!!A!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1117782528</Property>
		</Item>
		<Item Name="Get or Create Folder.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/BaseAccessorScripter/Get or Create Folder.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'^!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"Z!=!!)!!!!3A!!%5:P&lt;'2F=C"3:7:O&gt;7UA&lt;X6U!&amp;B!=!!?!!!V&amp;EVF&lt;7*F=F:*1X*F982J&lt;WYO&lt;(:M;7)=1G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)O&lt;(:D&lt;'&amp;T=Q!91G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!"B!)2*$=G6B&gt;'6"=V"S&lt;X"F=H2J:8-!!"R!-0````]46GFS&gt;(6B&lt;#"'&lt;WRE:8)A4G&amp;N:1"71(!!(A!!.2:.:7VC:8*735.S:7&amp;U;7^O,GRW&lt;'FC(%*B=W6"9W.F=X.P=F.D=GFQ&gt;'6S,GRW9WRB=X-!&amp;U*B=W6"9W.F=X.P=F.D=GFQ&gt;'6S)'FO!&amp;1!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!A!#1!+!Q!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!#!!!!2)!!!#3!!!!!!%!#Q!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1117782544</Property>
		</Item>
		<Item Name="WouldCrowdProperty.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/BaseAccessorScripter/WouldCrowdProperty.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%H!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"*!)1R8&lt;X6M:#"D=G^X:$]!!"Z!=!!)!!!!3A!!%5:P&lt;'2F=C"3:7:O&gt;7UA&lt;X6U!":!5!!$!!!!!1!##'6S=G^S)%F0!!!11#%,6W&amp;O&gt;(-A6X*J&gt;'5!$E!B#6&gt;B&lt;H2T5G6B:!!G1(!!#!!!!%I!!"F1=G^Q&lt;X.F:#"'&lt;WRE:8)A5G6G&lt;H6N)'FO!&amp;1!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!A!#1!+!Q!!?!!!$1A!!!!!!!!.#1!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!%A!!!"!!!!!1!!!!!!%!#Q!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1117782544</Property>
		</Item>
	</Item>
	<Item Name="Private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="Templates" Type="Folder">
			<Item Name="WriteTemplate_NoErrorTerminals.vit" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/BaseAccessorScripter/WriteTemplate_NoErrorTerminals.vit">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!$,!!!!"!!%!!!!-%"Q!"Y!!!Z-97*73568)%^C;G6D&gt;!!72%Z-8URB9F:*26=A4W*K:7.U)'^V&gt;!!!,E"Q!"Y!!!Z-97*73568)%^C;G6D&gt;!!62%Z-8URB9F:*26=A4W*K:7.U)'FO!'%!]!!-!!!!!!!!!!%!!!!!!!!!!!!!!!!!!!!#!A!!?!!!!!!!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!)!!!.!!!!$!!!!!!!!!!!!!!"!!-!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">3</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1082917376</Property>
			</Item>
			<Item Name="ReadTemplate_NoErrorTerminals.vit" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/BaseAccessorScripter/ReadTemplate_NoErrorTerminals.vit">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!$,!!!!"!!%!!!!-%"Q!"Y!!!Z-97*73568)%^C;G6D&gt;!!72%Z-8URB9F:*26=A4W*K:7.U)'^V&gt;!!!,E"Q!"Y!!!Z-97*73568)%^C;G6D&gt;!!62%Z-8URB9F:*26=A4W*K:7.U)'FO!'%!]!!-!!!!!!!!!!%!!!!!!!!!!!!!!!!!!!!#!A!!?!!!!!!!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!)!!!.!!!!$!!!!!!!!!!!!!!"!!-!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">3</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1082917376</Property>
			</Item>
			<Item Name="ReadTemplate.vit" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/BaseAccessorScripter/ReadTemplate.vit">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%I!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$"!=!!?!!!/4'&amp;C6EF&amp;6S"09GJF9X1!&amp;E2/4&amp;^-97*73568)%^C;G6D&gt;#"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!,E"Q!"Y!!!Z-97*73568)%^C;G6D&gt;!!62%Z-8URB9F:*26=A4W*K:7.U)'FO!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"!!(!A!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!!!!!!)!!!.!!!!$!!!!!!!!!!!!!!"!!A!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1343103488</Property>
			</Item>
			<Item Name="WriteTemplate.vit" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/BaseAccessorScripter/WriteTemplate.vit">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%I!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$"!=!!?!!!/4'&amp;C6EF&amp;6S"09GJF9X1!&amp;E2/4&amp;^-97*73568)%^C;G6D&gt;#"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!,E"Q!"Y!!!Z-97*73568)%^C;G6D&gt;!!62%Z-8URB9F:*26=A4W*K:7.U)'FO!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"!!(!A!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!!!!!!)!!!.!!!!$!!!!!!!!!!!!!!"!!A!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">11</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1082393088</Property>
			</Item>
		</Item>
		<Item Name="AddControlToWriteVI.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/BaseAccessorScripter/AddControlToWriteVI.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;Y!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;B!=!!?!!!V&amp;EVF&lt;7*F=F:*1X*F982J&lt;WYO&lt;(:M;7)=1G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)O&lt;(:D&lt;'&amp;T=Q!91G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!&amp;:!=!!?!!!V&amp;EVF&lt;7*F=F:*1X*F982J&lt;WYO&lt;(:M;7)=1G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)O&lt;(:D&lt;'&amp;T=Q!81G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)A;7Y!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">8192</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1117782544</Property>
		</Item>
		<Item Name="AddIndicatorToReadVI.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/BaseAccessorScripter/AddIndicatorToReadVI.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;Y!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;B!=!!?!!!V&amp;EVF&lt;7*F=F:*1X*F982J&lt;WYO&lt;(:M;7)=1G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)O&lt;(:D&lt;'&amp;T=Q!91G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!&amp;:!=!!?!!!V&amp;EVF&lt;7*F=F:*1X*F982J&lt;WYO&lt;(:M;7)=1G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)O&lt;(:D&lt;'&amp;T=Q!81G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)A;7Y!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">8192</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1117782544</Property>
		</Item>
		<Item Name="AddVIsToClass.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/BaseAccessorScripter/AddVIsToClass.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'9!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;B!=!!?!!!V&amp;EVF&lt;7*F=F:*1X*F982J&lt;WYO&lt;(:M;7)=1G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)O&lt;(:D&lt;'&amp;T=Q!91G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!#"!=!!)!!!!3A!!%E2F=X2J&lt;G&amp;U;7^O)%:P&lt;'2F=A!!6E"Q!"Y!!$57476N9G6S6EF$=G6B&gt;'FP&lt;CZM&gt;GRJ9BR#98.F17.D:8.T&lt;X*49X*J=(2F=CZM&gt;G.M98.T!"&gt;#98.F17.D:8.T&lt;X*49X*J=(2F=C"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!-!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!3!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">134217728</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342972432</Property>
		</Item>
		<Item Name="AddVIToClass.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/BaseAccessorScripter/AddVIToClass.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(H!!!!$Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!)E"Q!!A!!!"+!!!54G6X)("S&lt;WJF9X1A;82F&lt;3"S:79!!"J!=!!)!!!!!A!!$8:J)(*F:GZV&lt;3"P&gt;81!*E"Q!!A!!!"6!!!:4&amp;:$&lt;'&amp;T=URJ9H*B=HEA5G6G&lt;H6N)'^V&gt;!!%!!!!)%"Q!!A!!!"+!!!32'6T&gt;'FO982J&lt;WYA2G^M:'6S!!"5!0%!!!!!!!!!!B:.:7VC:8*735.S:7&amp;U;7^O,GRW&lt;'FC%5.-5V6*5&amp;^"9W.F=X-O9X2M!#.!&amp;A!$"&amp;*F971&amp;6X*J&gt;'5$5C^8!!!'17.D:8.T!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!"J!-0````]15(*P='^T:71A6EEA4G&amp;N:1!!'E"Q!!A!!!!#!!!-&gt;GEA=G6G&lt;H6N)'FO!!!A1(!!#!!!!&amp;5!!"*-6E.M98.T4'FC=G&amp;S?3"3:79!!&amp;1!]!!-!!-!"!!&amp;!!9!"Q!(!!A!#1!+!!M!$!!.!Q!!?!!!$1A!!!E!!!!.#A!!$1M!!!!!!!!!!!!!%A!!!!A!!!!+!!!"#A!!!!A!!!!+!!!!!!%!$A!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082143248</Property>
		</Item>
		<Item Name="CosmeticCleanup.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/BaseAccessorScripter/CosmeticCleanup.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;Y!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;B!=!!?!!!V&amp;EVF&lt;7*F=F:*1X*F982J&lt;WYO&lt;(:M;7)=1G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)O&lt;(:D&lt;'&amp;T=Q!91G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!&amp;:!=!!?!!!V&amp;EVF&lt;7*F=F:*1X*F982J&lt;WYO&lt;(:M;7)=1G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)O&lt;(:D&lt;'&amp;T=Q!81G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)A;7Y!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=#!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!"!!!!U!!!!-!!!!!!!!!!!!!!%!#!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">8192</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1117782528</Property>
		</Item>
		<Item Name="GetDistinctName.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/BaseAccessorScripter/GetDistinctName.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%R!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!":!-0````].17.D:8"U:71A&lt;G&amp;N:1!G1(!!#!!!!&amp;5!!"F-6E.M98.T4'FC=G&amp;S?3"3:7:O&gt;7UA&lt;X6U!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!&amp;E!Q`````QV1=G^Q&lt;X.F:#"O97VF!#*!=!!)!!!!61!!&amp;5R71WRB=X.-;7*S98*Z)&amp;*F:GZV&lt;1"5!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!A!#1-!!(A!!!U)!!!!!!!!$1I!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!%+!!!!#A!!!!!"!!I!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1117782544</Property>
		</Item>
		<Item Name="InstantiateAccessorVIs.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/BaseAccessorScripter/InstantiateAccessorVIs.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'5!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;B!=!!?!!!V&amp;EVF&lt;7*F=F:*1X*F982J&lt;WYO&lt;(:M;7)=1G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)O&lt;(:D&lt;'&amp;T=Q!91G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)A&lt;X6U!!!=1#%837ZD&lt;(6E:3"F=H*P=C"U:8*N;7ZB&lt;(-!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"71(!!(A!!.2:.:7VC:8*735.S:7&amp;U;7^O,GRW&lt;'FC(%*B=W6"9W.F=X.P=F.D=GFQ&gt;'6S,GRW9WRB=X-!&amp;U*B=W6"9W.F=X.P=F.D=GFQ&gt;'6S)'FO!'%!]!!-!!-!"!!%!!5!"!!%!!1!"A!(!!1!"!!)!Q!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!")!!!!+!!!!!!!!!!!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!E!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">8192</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1117782544</Property>
		</Item>
		<Item Name="SetUpClassControls.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/BaseAccessorScripter/SetUpClassControls.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'%!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;*!=!!?!!!V&amp;EVF&lt;7*F=F:*1X*F982J&lt;WYO&lt;(:M;7)=1G&amp;T:5&amp;D9W6T=W^S5W.S;8"U:8)O&lt;(:D&lt;'&amp;T=Q!35W.S;8"U:8)A1WRB=X-A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!"J!=!!)!!!!!A!!$&amp;:*)&amp;*F:G6S:7ZD:1!!4E"Q!"Y!!$57476N9G6S6EF$=G6B&gt;'FP&lt;CZM&gt;GRJ9BR#98.F17.D:8.T&lt;X*49X*J=(2F=CZM&gt;G.M98.T!!Z49X*J=(2F=C"$&lt;'&amp;T=Q!!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!(!!A$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!#!!!!!A!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">8192</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1117782544</Property>
		</Item>
		<Item Name="DeleteSeqStruct.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/BaseAccessorScripter/DeleteSeqStruct.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"U!!!!!Q!%!!!!)%"Q!!A!!%!7!!!4486M&gt;'FG=G&amp;N:3"4&gt;(6D&gt;#"J&lt;A")!0!!#A!!!!!!!!!!!!!!!!!!!!!!!!!"!Q!!U!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!%A!!!!!"!!)!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082655248</Property>
		</Item>
		<Item Name="Get Property Folder if Exists.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/BaseAccessorScripter/Get Property Folder if Exists.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;.!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!(E"Q!!A!!!"+!!!22G^M:'6S)&amp;*F:GZV&lt;3"P&gt;81!"!!!!#J!=!!)!!!!3A!!(6"B=G6O&gt;#"1=G^K:7.U382F&lt;3"3:7:O&gt;7UA&lt;X6U!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!(E!Q`````R21=G^Q:8*U?3"'&lt;WRE:8)A4G&amp;N:1!!+E"Q!!A!!!"+!!!=5'&amp;S:7ZU)&amp;"S&lt;WJF9X2*&gt;'6N)&amp;*F:GZV&lt;3"J&lt;A!!6!$Q!!Q!!Q!%!!5!"A!&amp;!!5!"1!&amp;!!=!#!!&amp;!!E$!!"Y!!!.#!!!#1!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!%3!!!!!!!!!"!!!!!!!1!+!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1117782544</Property>
		</Item>
		<Item Name="Add or Get Property Folder.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/BaseAccessorScripter/Add or Get Property Folder.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;0!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!(E"Q!!A!!!"+!!!22G^M:'6S)&amp;*F:GZV&lt;3"P&gt;81!$E!B#%.S:7&amp;U:71`!!!%!!!!(E!Q`````R21=G^Q:8*U?3"'&lt;WRE:8)A4G&amp;N:1!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!11#%+6W&amp;O&gt;(.8=GFU:1!!$E!B#6&gt;B&lt;H2T5G6B:!!K1(!!#!!!!%I!!"R198*F&lt;H1A5(*P;G6D&gt;%FU:7UA5G6G&lt;H6N)'FO!!"5!0!!$!!$!!1!"1!'!!9!"A!(!!9!#!!*!!I!#Q-!!(A!!!U)!!!.#Q!!#1!!!!!!!!!!!!!!!!!!!AA!!!!!!!!!#A!!!!I!!!!+!!!!#A!!!!!"!!Q!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1117782544</Property>
		</Item>
		<Item Name="Get Virtual Folder if Exists.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/BaseAccessorScripter/Get Virtual Folder if Exists.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;$!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!(E"Q!!A!!!"+!!!22G^M:'6S)&amp;*F:GZV&lt;3"P&gt;81!"!!!!#:!=!!)!!!!61!!'5R71WRB=X.-;7*S98*Z)&amp;*F:GZV&lt;3"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!=1$$`````%V:J=H2V97QA2G^M:'6S)%ZB&lt;75!*E"Q!!A!!!"6!!!94&amp;:$&lt;'&amp;T=URJ9H*B=HEA5G6G&lt;H6N)'FO!!"5!0!!$!!$!!1!"1!'!!5!"1!&amp;!!5!"Q!)!!5!#1-!!(A!!!U)!!!*!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!2)!!!!!!!!!%!!!!!!"!!I!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1117782544</Property>
		</Item>
		<Item Name="Add or Get Virtual Folder.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/LVClassLibrary/NewAccessors/BaseAccessorScripter/Add or Get Virtual Folder.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;$!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!(E"Q!!A!!!"+!!!22G^M:'6S)&amp;*F:GZV&lt;3"P&gt;81!"!!!!#:!=!!)!!!!61!!'5R71WRB=X.-;7*S98*Z)&amp;*F:GZV&lt;3"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!=1$$`````%V:J=H2V97QA2G^M:'6S)%ZB&lt;75!*E"Q!!A!!!"6!!!94&amp;:$&lt;'&amp;T=URJ9H*B=HEA5G6G&lt;H6N)'FO!!"5!0!!$!!$!!1!"1!'!!5!"1!&amp;!!5!"Q!)!!5!#1-!!(A!!!U)!!!*!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!2)!!!!!!!!!%!!!!!!"!!I!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1117782544</Property>
		</Item>
	</Item>
</LVClass>
